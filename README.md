# Test Technique Symfony [FUTURMAP]

Application de gestion de produit

Symfony (Version 5.2)

## Prerequis

- composer
- php v7.4
- extension sqlite de php activé

## Installation

composer install

## Lancer le projet

- cd SymfonyAppFuturmap
- php -s localhost:8000 -t public\

## Utilisation

=> http:localhost:8000

- username : admin
- password : demo


#############
## GraphQl ##
#############

=> http:localhost:8000/api/graphql

# Lister les produits
{
  products {
    edges {
      node {
        id,
        name,
        quantity,
        price
      }
    }
  }
}

# Récupérer un produit
{
  product (id: "/api/products/11") {
    name,
    quantity,
    price,
    category {
      name
    }
  }
}

# Créer une catégorie
mutation {
  createCategory (input:{name: "Catégorie test graphql"}) {
    category {
      id
    }
  }
}