<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductSearch;
use App\Form\ProductFormType;
use App\Form\ProductSearchType;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    private $repo;
    private $catrepo;

    public function __construct(ProductRepository $repo, CategoryRepository $catrepo) {
        $this->repo = $repo;
        $this->catrepo = $catrepo;
    }


    /**
     * @Route("/", name="product.index")
     */
    public function index(Request $request)
    {
        $filter = new ProductSearch();
        $form = $this->createForm(ProductSearchType::class, $filter);
        $form->handleRequest($request);

        $products = $this->repo->getAllProductsOrByCategory($filter->getQuery() ? $filter->getQuery()->getId() : null);
        return $this->render('product/index.html.twig', [
            'products' => $products,
            'current_menu' => 'products',
            'form' => $form->createView()
        ]);
    }

    

    /**
     * @Route("/create", name="product.create")
     */
    public function create(Request $request, EntityManagerInterface $em)
    {
        $product = new Product();
        $categories = $this->catrepo->findAll();
        $form = $this->createForm(ProductFormType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em->persist($product);
            $em->flush();
            $this->addFlash('success', 'Le produit a été crée');
            return $this->redirectToRoute('product.index');
        }

        return $this->render('product/create.html.twig', [
            'product' => $product,
            'categories' => $categories,
            'form' => $form->createView(),
            'current_menu' => 'products'
        ]);
    }

    /**
     * @Route("/edit/{id}", name="product.edit")
     */
    public function edit(Product $product, Request $request)
    {
        $categories = $this->catrepo->findAll();
        $form = $this->createForm(ProductFormType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->repo->add($product, true);
            $this->addFlash('success', 'Le produit a été modifié');
            return $this->redirectToRoute('product.index');
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'categories' => $categories,
            'form' => $form->createView(),
            'current_menu' => 'products'
        ]);
    }

    /**
     * @Route("/destroy/{id}", name="product.destroy")
     */
    public function destroy(Product $product, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $product->getId(), $request->get('_token'))) {
            $this->repo->remove($product, true);
            $this->addFlash('success', 'Le produit a été supprimé');
            return $this->redirectToRoute('product.index');
        }
    }

}