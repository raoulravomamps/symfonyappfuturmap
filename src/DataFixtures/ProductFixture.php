<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i=0; $i < 10; $i++) { 
            $product = new Product();

            $product
                ->setName($faker->word)
                ->setQuantity($faker->numberBetween(0, 50))
                ->setPrice($faker->numberBetween(10000, 500000));
            $manager->persist($product);
        }

        $manager->flush();
    }
}
