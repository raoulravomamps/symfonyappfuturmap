<?php

namespace App\Entity;

use App\Entity\Category;

class ProductSearch
{

    private Category $query;

    public function getQuery(): ?Category
    {
        return $this->query ?? null;
    }

    public function setQuery(Category $query): ProductSearch
    {
        $this->query = $query;

        return $this;
    }

}